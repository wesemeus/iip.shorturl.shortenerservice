﻿using Microsoft.EntityFrameworkCore;

namespace IIP.ShortUrl.ShortenerService.Data;

public sealed class ApplicationDbContext: DbContext
{
    public DbSet<Models.ShortUrl> ShortUrls { get; set; } = null!;

    public ApplicationDbContext()
    {
    }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
        Database.Migrate();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
    }
}