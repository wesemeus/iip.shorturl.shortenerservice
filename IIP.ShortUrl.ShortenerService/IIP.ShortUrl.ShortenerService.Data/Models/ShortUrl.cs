﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace IIP.ShortUrl.ShortenerService.Data.Models;

public sealed class ShortUrl
{
    [BsonId]
    public ObjectId Id { get; set; }
    
    public string Hash { get; set; } = null!;

    public string Url { get; set; } = null!;
}