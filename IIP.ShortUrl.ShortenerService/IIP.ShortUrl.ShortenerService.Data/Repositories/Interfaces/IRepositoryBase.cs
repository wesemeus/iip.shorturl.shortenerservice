﻿using MongoDB.Driver;

namespace IIP.ShortUrl.ShortenerService.Data.Repositories.Interfaces;

public interface IRepositoryBase<T> where T: class
{
    Task<List<T>> GetListAsync(FilterDefinition<T> selectFunc);

    Task<T?> GetFirstOrDefaultAsync(FilterDefinition<T> selectFunc);

    Task<T> AddAsync(T model);
}