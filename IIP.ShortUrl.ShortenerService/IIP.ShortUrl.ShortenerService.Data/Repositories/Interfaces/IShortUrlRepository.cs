﻿namespace IIP.ShortUrl.ShortenerService.Data.Repositories.Interfaces;

public interface IShortUrlRepository: IRepositoryBase<Models.ShortUrl>
{
}