﻿using IIP.ShortUrl.ShortenerService.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;

namespace IIP.ShortUrl.ShortenerService.Data.Repositories;

public class RepositoryBase<T>: IRepositoryBase<T> where T: class
{
    private readonly DbSet<T> _dbSet;
    private readonly IMongoClient _mongoClient;
    private readonly string DbName = "iip_shorturl";
    
    public string CollectionName { get; }

    public RepositoryBase(string collectionName)
    {
        CollectionName = collectionName;
        _mongoClient = new MongoClient("mongodb://192.168.0.109:27017,192.168.0.109:27018,192.168.0.109:27019?connect=replicaSet");
    }

    public async Task<List<T>> GetListAsync(FilterDefinition<T> selectFunc)
    {
        var db = _mongoClient.GetDatabase(DbName);
        var collection = db.GetCollection<T>(CollectionName);
        if (collection is null)
        {
            await db.CreateCollectionAsync(CollectionName);
        }
        var result = await (await collection!.FindAsync<T>(selectFunc)).ToListAsync();
        return result;
    }

    public async Task<T?> GetFirstOrDefaultAsync(FilterDefinition<T> selectFunc)
    {
        var db = _mongoClient.GetDatabase(DbName);
        var collection = db.GetCollection<T>(CollectionName);
        if (collection is null)
        {
            await db.CreateCollectionAsync(CollectionName);
        }
        var result = await (await collection!.FindAsync<T>(selectFunc)).FirstOrDefaultAsync();
        return result;
    }

    public async Task<T> AddAsync(T model)
    {
        var db = _mongoClient.GetDatabase(DbName);
        var collection = db.GetCollection<T>(CollectionName);
        if (collection is null)
        {
            await db.CreateCollectionAsync(CollectionName);
        }
        await collection!.InsertOneAsync(model);
        return model;
    }
}