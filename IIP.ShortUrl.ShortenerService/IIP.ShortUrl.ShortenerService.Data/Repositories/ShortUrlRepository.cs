﻿using IIP.ShortUrl.ShortenerService.Data.Repositories.Interfaces;

namespace IIP.ShortUrl.ShortenerService.Data.Repositories;

public sealed class ShortUrlRepository: RepositoryBase<Models.ShortUrl>, IShortUrlRepository
{
    private static string CollectionName => "short_url";
    
    public ShortUrlRepository() : base(CollectionName)
    {
    }
}