﻿using IIP.Dto.Requests.CreateShortUrl;
using IIP.Dto.Requests.GetUrlByHash;

namespace IIP.ShortUrl.ShortenerService.Logic.Services.Interfaces;

public interface IShortenerService
{
    Task<CreateShortUrlResponse> CreateShortUrlAsync(CreateShortUrlRequest request);

    Task<GetUrlByHashResponse> GetUrlByHashAsync(GetUrlByHashRequest request);
}