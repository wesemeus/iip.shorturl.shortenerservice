﻿using System.Text;
using IIP.Dto.Producers;
using IIP.Dto.Requests.CreateShortUrl;
using IIP.Dto.Requests.GetUrlByHash;
using IIP.Dto.Requests.SaveUrlToCache;
using IIP.ShortUrl.ShortenerService.Data.Repositories.Interfaces;
using IIP.ShortUrl.ShortenerService.Logic.Services.Interfaces;
using MongoDB.Driver;

namespace IIP.ShortUrl.ShortenerService.Logic.Services;

public sealed class ShortenerService: IShortenerService
{
    private readonly IShortUrlRepository _shortUrlRepository;
    private const int ShortUrlLength = 7;
    private const string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    private readonly ISaveUrlToCacheProducer _saveUrlToCacheProducer;

    public ShortenerService(ISaveUrlToCacheProducer saveUrlToCacheProducer,
        IShortUrlRepository shortUrlRepository)
    {
        _saveUrlToCacheProducer = saveUrlToCacheProducer;
        _shortUrlRepository = shortUrlRepository;
    }

    public async Task<CreateShortUrlResponse> CreateShortUrlAsync(CreateShortUrlRequest request)
    {
        if (!Uri.TryCreate(request.Url, UriKind.Absolute, out _))
        {
            throw new InvalidOperationException("it is not an url");
        }
        var hash = await GetHashString(request.Url);
        await _saveUrlToCacheProducer.RequestAsync(new SaveUrlToCacheRequest()
        {
            Url = request.Url,
            Hash = hash
        });
        return new CreateShortUrlResponse()
        {
            ShortUrl = hash
        };
    }

    public async Task<GetUrlByHashResponse> GetUrlByHashAsync(GetUrlByHashRequest request)
    {
        var filter = new FilterDefinitionBuilder<Data.Models.ShortUrl>().Eq(x => x.Hash, request.Hash);
        var url = await _shortUrlRepository.GetFirstOrDefaultAsync(filter);
        return await Task.FromResult(new GetUrlByHashResponse()
        {
            Url = url?.Url
        });
    }
    
    private async Task<string> GetHashString(string inputString)
    {
        var existFilter = new FilterDefinitionBuilder<Data.Models.ShortUrl>().Eq(x => x.Url, inputString);
        var existUrl = await _shortUrlRepository.GetFirstOrDefaultAsync(existFilter);
        if (existUrl is not null)
        {
            return existUrl.Hash;
        }
        
        var sb = new StringBuilder();
        var rnd = new Random();
        while (true)
        {
            for (var i = 0; i < ShortUrlLength; i++)
            {
                sb.Append(Alphabet[rnd.Next(Alphabet.Length - 1)]);
            }

            var resultHash = sb.ToString();
            var filter = new FilterDefinitionBuilder<Data.Models.ShortUrl>().Eq(x => x.Hash, resultHash);
            var existHash = await _shortUrlRepository.GetFirstOrDefaultAsync(filter);
            if (existHash is null)
            {
                await _shortUrlRepository.AddAsync(new Data.Models.ShortUrl()
                {
                    Hash = resultHash,
                    Url = inputString
                });
                return resultHash;
            }

            sb.Clear();
        }
    }
}