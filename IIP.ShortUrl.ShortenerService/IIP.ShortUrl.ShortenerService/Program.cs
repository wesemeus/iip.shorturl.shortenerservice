using IIP.Dto.Producers;
using IIP.ShortUrl.ShortenerService.Data.Repositories;
using IIP.ShortUrl.ShortenerService.Data.Repositories.Interfaces;
using IIP.ShortUrl.ShortenerService.Logic.RabbitMq.Producers;
using IIP.ShortUrl.ShortenerService.Logic.Services;
using IIP.ShortUrl.ShortenerService.Logic.Services.Interfaces;
using MassTransit;
using Prometheus;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<IShortenerService, ShortenerService>();
builder.Services.AddScoped<ISaveUrlToCacheProducer, SaveUrlToCacheProducer>();
builder.Services.AddScoped<IShortUrlRepository, ShortUrlRepository>();

builder.Services.AddMassTransit(cfg =>
{
    cfg.SetKebabCaseEndpointNameFormatter();
    cfg.AddConsumers(typeof(Program).Assembly);
    cfg.UsingRabbitMq((ctx, c) =>
    {
        c.Host("localhost", "/", h => {
            h.Username("guest");
            h.Password("guest");
            h.UseCluster(cluster =>
            {
                cluster.Node("localhost:5672");
                cluster.Node("localhost:5673");
            });
        });

        c.ConfigureEndpoints(ctx);
    });
});

var app = builder.Build();

app.UseHttpMetrics();
app.MapMetrics();

app.Run();