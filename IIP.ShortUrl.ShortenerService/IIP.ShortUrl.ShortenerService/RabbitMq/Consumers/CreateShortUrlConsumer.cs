﻿using IIP.Dto.Requests.CreateShortUrl;
using IIP.ShortUrl.ShortenerService.Logic.Services.Interfaces;
using MassTransit;

namespace IIP.ShortUrl.ShortenerService.RabbitMq.Consumers;

public sealed class CreateShortUrlConsumer: IConsumer<CreateShortUrlRequest>
{
    private readonly IShortenerService _shortenerService;

    public CreateShortUrlConsumer(IShortenerService shortenerService)
    {
        _shortenerService = shortenerService;
    }

    public async Task Consume(ConsumeContext<CreateShortUrlRequest> context)
    {
        var result = await _shortenerService.CreateShortUrlAsync(context.Message);
        await context.RespondAsync(result);
    }
}