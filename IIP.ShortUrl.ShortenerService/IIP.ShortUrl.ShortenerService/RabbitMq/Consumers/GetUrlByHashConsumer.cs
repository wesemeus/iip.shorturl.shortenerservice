﻿using IIP.Dto.Requests.GetUrlByHash;
using IIP.ShortUrl.ShortenerService.Logic.Services.Interfaces;
using MassTransit;

namespace IIP.ShortUrl.ShortenerService.RabbitMq.Consumers;

public sealed class GetUrlByHashConsumer: IConsumer<GetUrlByHashRequest>
{
    private readonly IShortenerService _shortenerService;

    public GetUrlByHashConsumer(IShortenerService shortenerService)
    {
        _shortenerService = shortenerService;
    }

    public async Task Consume(ConsumeContext<GetUrlByHashRequest> context)
    {
        var result = await _shortenerService.GetUrlByHashAsync(context.Message);
        await context.RespondAsync(result);
    }
}