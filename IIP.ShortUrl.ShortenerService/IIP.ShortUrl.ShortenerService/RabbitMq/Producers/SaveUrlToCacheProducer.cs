﻿using IIP.Dto.Producers;
using IIP.Dto.Requests.SaveUrlToCache;
using MassTransit;

namespace IIP.ShortUrl.ShortenerService.Logic.RabbitMq.Producers;

public sealed class SaveUrlToCacheProducer: ISaveUrlToCacheProducer
{
    private readonly IRequestClient<SaveUrlToCacheRequest> _bus;

    public SaveUrlToCacheProducer(IRequestClient<SaveUrlToCacheRequest> bus)
    {
        _bus = bus;
    }

    public async Task<SaveUrlToCacheResponse> RequestAsync(SaveUrlToCacheRequest request)
    {
        var result = await _bus.GetResponse<SaveUrlToCacheResponse>(request);
        return result.Message;
    }
}